﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;
using System.Collections.ObjectModel;
using System.Collections.Immutable;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.CompilerServices;
using VRage.Game.Utils;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        /*
         * SolarMap 
         * 
         * -----------
         * 
         * Version: 		0.9
         * Author: 		Skimt
         * Date: 			July 13 2020
         * Description: 
         * 
         * The solar map will project the world coordinates of planets and the grid unto 
         * a text panel in the form of a 2D map, along with detailed information regarding
         * the planets and moons. Moons are not visible on the map, nor is the planet
         * 'Triton' visible on the map or in the information panel. No plans have been made
         * to develop this script further unless boredom sets in.
         * 
         * -----------
         * 
         * INSTALLATION:
         * 
         * Add "[SolarMap]" without the citation marks into the CustomData of a text panel.
         * The script should automatically pick up new text panels, although it might take
         * a while because the script has been deliberately written to operate efficiently.
         */

        private const bool Emitter = true;
        private const UpdateFrequency FREQUENCY = UpdateFrequency.Update10;

        private readonly IEnumerator<bool> terminalCycle;
        private readonly ProgrammableBlock programmableBlock;
        //private readonly ShipController shipController;
        private readonly TextPanel textPanel;
        private readonly InterstellarCom comm;

        public Program()
        {
            var textPanelConfiguration = new TextPanelConfiguration()
            {
                OrbitColor = Color.Blue,
                BackGroundColor = Color.Black,
                PlanetsColor = Color.White,
                MoonsColor = Color.Green,
                MapOffsetUnit = 1300000,
                ZoomUnit=0.2f,
                AngleUnit=5f,
                DisplayInformationPanel = false,
                DisplayMap = true,
                DisplayInterstellarCommunicationBodies = true,
                DisplayMoons = false,
                DisplayMoonNames = true,
                DisplayMoonDistances = false,
                DisplayOrbits = true,
                DisplayPlanets = true,
                DisplayPlanetNames = true,
                DisplayPlanetDistances = false,
            };
            comm = new InterstellarCom(IGC, this);
            // ---------------------------------------------------------------
            // Celestial bodies - Start.
            // ---------------------------------------------------------------
            List<CelestialBody> celestialBodies = new List<CelestialBody>()
                    {
                    new CelestialBody
                    {
                        Name = "EarthLike",
                        Radius = 60000,
                        Gravity = 1,
                        HasAtmosphere = true,
                        Oxygen = Oxygen.High,
                        Type = CelestialType.Planet,
                        Position = new Vector3(0f, 0f, 0f),
                        Resources = "All"
                    },
                    new CelestialBody
                    {
                        Name = "Moon",
                        Radius = 9500,
                        Gravity = 0.25f,
                        HasAtmosphere = false,
                        Oxygen = Oxygen.None,
                        Type = CelestialType.Moon,
                        Position = new Vector3(16384.5f, 136384.5f, -113615.5f),
                        Resources = "All"
                    },
                    new CelestialBody
                    {
                        Name = "Mars",
                        Radius = 60000,
                        Gravity = 0.9f,
                        HasAtmosphere = true,
                        Oxygen = Oxygen.None,
                        Type = CelestialType.Planet,
                        Position = new Vector3(1031072.5f, 131072.5f, 1631072.5f),
                        Resources = "All"
                    },
                    new CelestialBody
                    {
                        Name = "Europa",
                        Radius = 9500,
                        Gravity = 0.25f,
                        HasAtmosphere = true,
                        Oxygen = Oxygen.None,
                        Type = CelestialType.Moon,
                        Position = new Vector3(916384.5f, 16384.5f, 1616384.5f),
                        Resources = "All"
                    },
                    new CelestialBody
                    {
                        Name = "Alien",
                        Radius = 60000,
                        Gravity = 1.1f,
                        HasAtmosphere = true,
                        Oxygen = Oxygen.Low,
                        Type = CelestialType.Planet,
                        Position = new Vector3(131072.5f, 131072.5f, 5731072.5f),
                        Resources = "All"
                    },
                    new CelestialBody
                    {
                        Name = "Titan",
                        Radius = 9500,
                        Gravity = 0.25f,
                        HasAtmosphere = true,
                        Oxygen = Oxygen.None,
                        Type = CelestialType.Moon,
                        Position = new Vector3(36384.5f, 226384.5f, 5796384.5f),
                        Resources = "All"
                    }
		            /*,
				        new CelestialBody
				        {
					        Name = "Triton",
					        Radius = 40126.5f,
					        Gravity = 1,
					        HasAtmosphere = true,
					        Oxygen = Oxygen.High,
					        Type = CelestialType.Planet,
					        Position = new Vector3(-284463.5f, -2434463.5, 365536,5f),
					        Resources = "All"
				        }
				        */
	                };
            // ---------------------------------------------------------------
            // Celestial bodies - End.
            // ---------------------------------------------------------------
            programmableBlock = new ProgrammableBlock(this, FREQUENCY);
            //shipController = new ShipController(this);
            textPanel = new TextPanel(this, new Map(celestialBodies), comm, textPanelConfiguration);

            terminalCycle = SetTerminalCycle();
        }
        private MyCommandLine commandLine = new MyCommandLine();

        public void Handle(string args)
        {
            if (commandLine.TryParse(args))
            {
                switch (commandLine.Argument(0))
                {
                    case "left":
                        textPanel.map.AddOffsetX(textPanel.textPanelConfiguration.MapOffsetUnit);
                        break;
                    case "right":
                        textPanel.map.AddOffsetX(-textPanel.textPanelConfiguration.MapOffsetUnit);
                        break;
                    case "up":
                        textPanel.map.AddOffsetY(textPanel.textPanelConfiguration.MapOffsetUnit);
                        break;
                    case "down":
                        textPanel.map.AddOffsetY(-textPanel.textPanelConfiguration.MapOffsetUnit);
                        break;
                    case "reset":
                        textPanel.map.Reset();
                        break;
                    case "zoom-in":
                        textPanel.map.Zoom += textPanel.textPanelConfiguration.ZoomUnit;
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "zoom-out":
                        textPanel.map.Zoom -= textPanel.textPanelConfiguration.ZoomUnit;
                        if (textPanel.map.Zoom < 0.1)
                            textPanel.map.Zoom = 0.1f;
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "pitch-up":
                        textPanel.map.pitchAngle += textPanel.textPanelConfiguration.AngleUnit *((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "pitch-down":
                        textPanel.map.pitchAngle -= textPanel.textPanelConfiguration.AngleUnit * ((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "yaw-up":
                        textPanel.map.yawAngle += textPanel.textPanelConfiguration.AngleUnit * ((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "yaw-down":
                        textPanel.map.yawAngle -= textPanel.textPanelConfiguration.AngleUnit * ((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "roll-up":
                        textPanel.map.rollAngle += textPanel.textPanelConfiguration.AngleUnit * ((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    case "roll-down":
                        textPanel.map.rollAngle -= textPanel.textPanelConfiguration.AngleUnit * ((float)Math.PI / 180);
                        textPanel.map.UpdateCelestialBodyCollectionToMapParameters();
                        break;
                    default:
                        return;
                }
            }
        }
        public enum CelestialType
        {
            Asteroid,
            Planet,
            Moon
        }

        public enum Oxygen
        {
            None,
            Low,
            High
        }

        private IEnumerator<bool> SetTerminalCycle()
        {
            while (true)
            {
                //yield return shipController.Run();
                yield return textPanel.Run();
            }
        }

        public void Main(string arg, UpdateType updateType)
        {
            // The update type is binary. Must look it up on Malware's wikia to figure out how to manipulate it.
            if ((updateType & Update[FREQUENCY]) != 0)
            {
                if (!terminalCycle.MoveNext())
                    terminalCycle.Dispose();
                programmableBlock.Draw(comm, textPanel.map);
                if (Emitter)
                {
                    comm.Sender();
                }
            }

            if ((updateType & UpdateType.IGC) != 0)
            {
                comm.Receiver(updateType);
            }

            if ((updateType & (UpdateType.Trigger | UpdateType.Terminal)) != 0)
            {
                Handle(arg);
            }

        }

        /// <summary>
        /// Common behavior to all terminals.
        /// </summary>
        abstract public class Terminal<T> where T : class, IMyTerminalBlock
        {

            protected Program program;
            private readonly List<T> list = new List<T>();
            private int listIndex, listUpdate;

            public Terminal(Program program)
            {
                this.program = program;
            }

            public bool IsListEmpty => list.Count == 0;

            public bool Run()
            {

                if (listIndex < list.Count)
                {

                    if (list.Count == 0)
                        return true;

                    if (!IsCorrupt(list[listIndex]))
                    {
                        OnCycle(list[listIndex]);
                    }
                    else
                    {
                        list.Remove(list[listIndex]);
                    }

                    listIndex++;

                }
                else
                {

                    // Update terminals of this type roughly every ~20 seconds when update frequency is 10.
                    if (listUpdate % (int)(60 / (list.Count + 1)) == 0)
                    {
                        program.GridTerminalSystem.GetBlocksOfType(list, Collect);
                    }

                    listUpdate++;
                    listIndex = 0;

                }

                return true;

            }

            /// <summary>
            /// Specificies which terminals to be collected. Use override.
            /// </summary>
            public virtual bool Collect(T terminal)
            {
                return terminal.IsSameConstructAs(program.Me);
            }

            public abstract void OnCycle(T terminal);

            /// <summary>
            /// Checks if the terminal is null, gone from world, or broken off from grid.
            /// </summary>
            public bool IsCorrupt(T block)
            {
                if (block == null || block.WorldMatrix == MatrixD.Identity) return true;
                return !(program.GridTerminalSystem.GetBlockWithId(block.EntityId) == block);
            }

        }

        public class CelestialBody
        {

            public CelestialType Type;
            public Oxygen Oxygen;
            public Vector3 Position;
            public Vector3 Orbit3DPostion;
            public Vector2 Original2DOrbitPosition;
            public bool HasAtmosphere;
            public float Radius;
            public float Gravity;
            public string Name;
            public string Resources;
            public Vector2 OrbitDiameter;

            // Map coordinates.
            public Vector2 OrbitPosition;
            public Vector2 PlanetPosition;
            public Vector2 OrbitSize;
            public Vector2 PlanetSize;
            public Vector2 LblTitlePos;
            public Vector2 LblDistancePos;

        }
        public class TextPanelConfiguration
        {
            public Color OrbitColor { get; set; } = Color.DarkGray;
            public Color PlanetsColor { get; set; } = Color.White;
            public Color MoonsColor { get; set; } = Color.White;
            public Color BackGroundColor { get; set; } = Color.Blue;
            public float MapOffsetUnit { get; set; } = 1000;
            public float ZoomUnit { get; set; } = 1f;
            public float AngleUnit { get; set; } = 5f;
            public bool DisplayInformationPanel { get; set; } = true;
            public bool DisplayMap { get; set; } = true;
            public bool DisplayOrbits { get; set; } = true;
            public bool DisplayPlanets { get; set; } = true;
            public bool DisplayPlanetNames { get; set; } = true;
            public bool DisplayPlanetDistances { get; set; } = true;
            public bool DisplayMoons { get; set; } = false;
            public bool DisplayMoonNames { get; set; } = false;
            public bool DisplayMoonDistances { get; set; } = false;
            public bool DisplayInterstellarCommunicationBodies { get; set; } = true;
            public bool DisplaySelfPosition { get; set; } = true;
        }
        private class Map
        {

            public Vector2 maxOffset = Vector2.Zero;
            public float maxDistance;
            private float size;
            public Vector2 mapCoordinates = Vector2.Zero;
            public Vector2 ManualOffset = new Vector2(0,0);
            public float yawAngle = 0;
            public float pitchAngle = 0;
            public float rollAngle = 0;
            public float Zoom { get; set; } = 1;

            public Map(List<CelestialBody> celestialBodies)
            {

                CelestialBodies = celestialBodies;
                CelestialInfo = new List<CelestialBody>(celestialBodies);
                AutoCenterMaps();
                UpdateCelestialBodyCollectionToMapParameters();
            }

            public List<CelestialBody> CelestialBodies { get; }
            public List<CelestialBody> CelestialInfo { get; }

            public void AutoCenterMaps()
            {
                foreach (CelestialBody planet in CelestialBodies)
                {
                    // Sets the maximum distance of the solar system.
                    maxDistance = planet.Position.X > maxDistance ? planet.Position.X : maxDistance;
                    maxDistance = planet.Position.Z > maxDistance ? planet.Position.Z : maxDistance;

                    // Sets the maximum offset from zero, to center the map.
                    maxOffset.X = planet.Position.X > maxOffset.X ? planet.Position.X : maxOffset.X;
                    maxOffset.Y = planet.Position.Z > maxOffset.Y ? planet.Position.Z : maxOffset.Y;

                }

                size = (maxDistance * 2);
            }
            public Vector2 GetMapPosition(Vector3 position)
            {
                position = position - CelestialBodies.Find(x => x.Name == "Mars").Position;

                Matrix3x3 yawMatrix = new Matrix3x3(
                    (float)Math.Cos(yawAngle), (float)-Math.Sin(yawAngle), 0,
                    (float)Math.Sin(yawAngle), (float)Math.Cos(yawAngle), 0,
                    0, 0, 1);

                Matrix3x3 pitchMatrix = new Matrix3x3(
                    (float)Math.Cos(pitchAngle), 0, (float)Math.Sin(pitchAngle),
                    0, 1, 0,
                    (float)-Math.Sin(pitchAngle), 0, (float)Math.Cos(pitchAngle));

                Matrix3x3 rollMatrix = new Matrix3x3(
                    1, 0, 0,
                    0, (float)Math.Cos(rollAngle), (float)-Math.Sin(rollAngle),
                    0, (float)Math.Sin(rollAngle), (float)Math.Cos(rollAngle));
                position = MatrixHelper.Multiply(position, MatrixHelper.Multiply(yawMatrix, MatrixHelper.Multiply(pitchMatrix, rollMatrix)));
                mapCoordinates.X = Zoom * position.X + size - maxOffset.X + ManualOffset.X;
                mapCoordinates.Y = Zoom * position.Z + size - maxOffset.Y + ManualOffset.Y;

                return Vector2.One - (mapCoordinates / (2*size));
            }

            public void UpdateCelestialBodyCollectionToMapParameters()
            {
                foreach (CelestialBody celestialBody in CelestialBodies)
                {

                    // Set planet position relative to a 2D surface.
                    //celestialBody.MapPosition = GetMapPosition(celestialBody.Position);

                    // Find nearest planet.
                    if (celestialBody.Type == CelestialType.Moon)
                    {
                        float distance = 0;
                        foreach (CelestialBody planet in CelestialBodies)
                        {
                            if (planet.Type == CelestialType.Planet)
                            {
                                float dist = Vector3.Distance(celestialBody.Position, planet.Position);
                                if (dist < distance)
                                {
                                    distance = dist;
                                }
                            }
                        }
                    }

                    // Additional data.
                    celestialBody.Original2DOrbitPosition = new Vector2(512);
                    celestialBody.PlanetPosition = celestialBody.Original2DOrbitPosition * GetMapPosition(celestialBody.Position);
                    //celestialBody.OrbitSize = new Vector2(Vector2.Distance(celestialBody.PlanetPosition, celestialBody.Original2DOrbitPosition)) * 2;
                    celestialBody.PlanetSize = celestialBody.Original2DOrbitPosition * celestialBody.Radius * Zoom * 0.000001f - 0.01f;
                    celestialBody.LblTitlePos = new Vector2(celestialBody.PlanetPosition.X, celestialBody.PlanetPosition.Y - celestialBody.Original2DOrbitPosition.Y * 0.1f);
                    celestialBody.LblDistancePos = new Vector2(celestialBody.PlanetPosition.X, celestialBody.PlanetPosition.Y - celestialBody.Original2DOrbitPosition.Y * 0.065f);
                    celestialBody.Orbit3DPostion = new Vector3(-10000000, celestialBody.PlanetPosition.Y, -10000000);
                    celestialBody.OrbitPosition = celestialBody.Original2DOrbitPosition * GetMapPosition(celestialBody.Orbit3DPostion);
                    //celestialBody.OrbitPosition = new Vector2(512);
                }

                CelestialBodies.Sort(SortByDistance);
                CelestialBodies.Sort(SortByType);
            }
            private int SortByDistance(CelestialBody a, CelestialBody b)
            {

                float distanceA = (new Vector2(a.Position.X, a.Position.Z) - Vector2.One).LengthSquared();
                float distanceB = (new Vector2(b.Position.X, b.Position.Z) - Vector2.One).LengthSquared();

                if (distanceA > distanceB)
                    return -1;
                else if (distanceB > distanceA)
                    return 1;
                return 0;

            }
            private int SortByType(CelestialBody a, CelestialBody b)
            {
                if (a.Type == CelestialType.Moon && b.Type == CelestialType.Planet)
                    return -1;
                else if (b.Type == CelestialType.Moon && a.Type == CelestialType.Planet)
                    return 1;
                return 0;
            }
            public void AddOffsetX(float x)
            {
                this.ManualOffset.X += x;
                UpdateCelestialBodyCollectionToMapParameters();
            }
            public void AddOffsetY(float y)
            {
                this.ManualOffset.Y += y;
                UpdateCelestialBodyCollectionToMapParameters();
            }
            public void Reset()
            {
                ManualOffset = new Vector2(0, 0);
                Zoom = 1;
                pitchAngle = 0;
                rollAngle = 0;
                yawAngle = 0;
                UpdateCelestialBodyCollectionToMapParameters();
            }
        }

        private class ShipController : Terminal<IMyShipController>
        {

            private IMyShipController main;

            public ShipController(Program program) : base(program) { }

            public IMyShipController Main
            {
                get
                {
                    return IsListEmpty || (main != null && (main.WorldMatrix == MatrixD.Identity || !main.IsWorking)) ? null : main;
                }
                set
                {
                    main = value;
                }
            }

            public override void OnCycle(IMyShipController terminal)
            {

                if (terminal.IsWorking)
                {
                    if (Main == null)
                    {
                        Main = terminal;
                    }
                    if (terminal.IsMainCockpit)
                    {
                        Main = terminal;
                    }
                }

            }

        }

        private class TextPanel : Terminal<IMyTextPanel>
        {

            private Vector2 infoSize = new Vector2(175, 20);
            private Vector2 infoPosition = new Vector2(7, 7);
            private Vector2 lcdSize = new Vector2(512);
            private Vector2 fiveThirdCenter = new Vector2(512 / 2, 307.2f / 2 * 1.6f);
            private Vector3 gridWorldPosition;

            private readonly InterstellarCom COM;
            public readonly TextPanelConfiguration textPanelConfiguration;
            public readonly Map map;

            public TextPanel(Program program, Map map, InterstellarCom comm, TextPanelConfiguration textPanelConfiguration) : base(program)
            {
                this.map = map;
                this.COM = comm;
                this.textPanelConfiguration = textPanelConfiguration;
            }

            public override void OnCycle(IMyTextPanel lcd)
            {

                gridWorldPosition = program.Me.GetPosition();
                lcd.ScriptBackgroundColor = this.textPanelConfiguration.BackGroundColor;
                using (MySpriteDrawFrame frame = lcd.DrawFrame())
                {
                    // Dimension error text.
                    if (lcd.SurfaceSize.Y != 512)
                    {
                        frame.Add(new MySprite(SpriteType.TEXT, "Solar map cannot be displayed on 5:3 text panel.", fiveThirdCenter, null, Color.White, null, rotation: 0.6f));
                        return;
                    }

                    if (this.textPanelConfiguration.DisplayMap)
                    {
                        if(this.textPanelConfiguration.DisplayOrbits)
                            DrawPlanetOrbits(frame, this.textPanelConfiguration.OrbitColor, this.textPanelConfiguration.BackGroundColor);
                        if(this.textPanelConfiguration.DisplayPlanets)
                            DrawPlanets(frame, this.textPanelConfiguration.PlanetsColor, this.textPanelConfiguration.BackGroundColor);
                        if (this.textPanelConfiguration.DisplayMoons)
                            DrawMoons(frame, this.textPanelConfiguration.MoonsColor, this.textPanelConfiguration.BackGroundColor);
                        if(this.textPanelConfiguration.DisplaySelfPosition)
                            DrawSelfPosition(frame);
                        if(this.textPanelConfiguration.DisplayInterstellarCommunicationBodies)
                            DrawInterstellarCommunicationBodies(frame);
                    }
                    if (this.textPanelConfiguration.DisplayInformationPanel)
                        DrawInformationPanel(frame);
                    // SHOW OMG
                    //float azimuth, elevation;
                    //Vector3.GetAzimuthAndElevation(program.shipController.Main.WorldMatrix.Forward, out azimuth, out elevation);
                    //frame.Add(new MySprite(SpriteType.TEXT, (azimuth).ToString(), new Vector2(250, 250), null, null, null, rotation: 0.7f));

                }
            }

            protected void DrawPlanetOrbits(MySpriteDrawFrame frame, Color orbitColor, Color backgroundColor)
            {
                // Celestial orbits.
                foreach (CelestialBody celestialBody in map.CelestialBodies.Where(cb => cb.Type == CelestialType.Planet))
                {
                    if(Vector2.Distance(celestialBody.OrbitDiameter, Vector2.Zero) == 0)
                        celestialBody.OrbitDiameter = new Vector2(Vector2.Distance(celestialBody.PlanetPosition, celestialBody.OrbitPosition)) * 2;
                    celestialBody.OrbitSize.X = celestialBody.OrbitDiameter.X * (float)Math.Cos(map.yawAngle);
                    celestialBody.OrbitSize.Y = celestialBody.OrbitDiameter.Y * (float)Math.Cos(map.rollAngle);
                    celestialBody.OrbitSize *= map.Zoom;

                    // Border and fill.
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.OrbitPosition, celestialBody.OrbitSize + 3, orbitColor));
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.OrbitPosition, celestialBody.OrbitSize, backgroundColor));
                }
            }
            protected void DrawPlanets(MySpriteDrawFrame frame, Color celestialBodyColor, Color backgroundColor)
            {
                // Celestial bodies.
                foreach (CelestialBody celestialBody in map.CelestialBodies.Where(cb => cb.Type == CelestialType.Planet))
                {

                    // Text.
                    if(this.textPanelConfiguration.DisplayPlanetNames)
                        frame.Add(new MySprite(SpriteType.TEXT, celestialBody.Name, celestialBody.LblTitlePos, null, null, null, rotation: 0.7f));
                    if(this.textPanelConfiguration.DisplayPlanetDistances)
                        frame.Add(new MySprite(SpriteType.TEXT, (Vector3.Distance(celestialBody.Position, gridWorldPosition) / 1000).ToString("F1") + " km", celestialBody.LblDistancePos, null, null, null, rotation: 0.55f));

                    // Border and fill.
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.PlanetPosition, celestialBody.PlanetSize + 3, celestialBodyColor));
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.PlanetPosition, celestialBody.PlanetSize, backgroundColor));

                }
            }
            protected void DrawMoons(MySpriteDrawFrame frame, Color celestialBodyColor, Color backgroundColor)
            {
                // Celestial bodies.
                foreach (CelestialBody celestialBody in map.CelestialBodies.Where(cb => cb.Type == CelestialType.Moon))
                {

                    // Text.
                    if(this.textPanelConfiguration.DisplayMoonNames)
                        frame.Add(new MySprite(SpriteType.TEXT, celestialBody.Name, celestialBody.LblTitlePos, null, null, null, rotation: 0.7f));
                    if(this.textPanelConfiguration.DisplayMoonDistances)
                        frame.Add(new MySprite(SpriteType.TEXT, (Vector3.Distance(celestialBody.Position, gridWorldPosition) / 1000).ToString("F1") + " km", celestialBody.LblDistancePos, null, null, null, rotation: 0.55f));

                    // Border and fill.
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.PlanetPosition, celestialBody.PlanetSize + 3, celestialBodyColor));
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", celestialBody.PlanetPosition, celestialBody.PlanetSize, backgroundColor));

                }
            }
            protected void DrawInformationPanel(MySpriteDrawFrame frame)
            {
                // Information panel background.
                frame.Add(new MySprite(SpriteType.TEXTURE, "SquareSimple", new Vector2(95, 256), new Vector2(183, 505), new Color(0, 0, 0, 50)));

                // Information panel content.
                for (int i = 0; i < map.CelestialInfo.Count; i++)
                {
                    CelestialBody cb = map.CelestialInfo[i];

                    // Title: Background and text.
                    frame.Add(new MySprite(SpriteType.TEXTURE, "SquareSimple", new Vector2(infoPosition.X + (infoSize.X / 2), (infoSize.Y / 2) + infoPosition.Y + (83 * i)), infoSize, new Color(0, 0, 0, 150)));
                    frame.Add(new MySprite(SpriteType.TEXT, cb.Name, new Vector2(3 + infoPosition.X, infoPosition.Y + (83 * i)), null, null, null, TextAlignment.LEFT, 0.6f));
                    frame.Add(new MySprite(SpriteType.TEXT, (Vector3.Distance(cb.Position, gridWorldPosition) / 1000).ToString("F1") + " km", new Vector2(172 + infoPosition.X, 2 + infoPosition.Y + (83 * i)), null, null, null, TextAlignment.RIGHT, 0.5f));

                    // Body: Content.
                    string txt = "Radius: " + (cb.Radius / 1000).ToString("F1") + " km\n" +
                                "Gravity: " + cb.Gravity.ToString("F1") + " G\n" +
                                "Atmosphere: " + cb.HasAtmosphere + "\n" +
                                "Oxygen: " + cb.Oxygen + "\n" +
                                "Resources: " + cb.Resources + "\n";
                    frame.Add(new MySprite(SpriteType.TEXT, txt, new Vector2(3 + infoPosition.X, infoPosition.Y + infoSize.Y + (83 * i)), null, null, null, TextAlignment.LEFT, 0.4f));
                }
            }
            protected void DrawSelfPosition(MySpriteDrawFrame frame)
            {
                Vector2 position = lcdSize * map.GetMapPosition(gridWorldPosition);
                /*if (program.shipController != null && program.shipController.Main != null && !program.Me.CubeGrid.IsStatic)
                {
                    // float rotation = -(float)(Math.Acos(program.shipController.Main.WorldMatrix.Forward.Z) + (Math.PI / 2f));
                    float azimuth, elevation;
                    Vector3.GetAzimuthAndElevation(program.shipController.Main.WorldMatrix.Forward, out azimuth, out elevation);
                    frame.Add(new MySprite(SpriteType.TEXTURE, "AH_BoreSight", position * positionMult, lcdSize * 0.05f + 3, Color.Red, null, rotation: -azimuth + (float)(Math.PI / 2f)));
                }
                else
                {*/
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", position, lcdSize * 0.01f, Color.Red));
                //}
            }
            protected void DrawInterstellarCommunicationBodies(MySpriteDrawFrame frame)
            {
                foreach (var ship in COM.interstellarModelCollection)
                {
                    Vector2 shipPosition = lcdSize * map.GetMapPosition(ship.ShipPosition);
                    frame.Add(new MySprite(SpriteType.TEXTURE, "Circle", shipPosition, lcdSize * 0.01f, Color.Green));
                    COM.checkKeepAlive(ship);
                }
            }

            public override bool Collect(IMyTextPanel terminal)
            {

                // Regularly sort the celestial information list.
                //map.CelestialInfo.Sort(SortByDistance);

                // Collect this.
                bool isSolarmap = terminal.IsSameConstructAs(program.Me) && terminal.CustomName.Contains("[SolarMap]");

                // Set the content type of this terminal to script.
                if (isSolarmap)
                    terminal.ContentType = ContentType.SCRIPT;

                // Return.
                return isSolarmap;

            }
            public int SortByDistance(CelestialBody a, CelestialBody b)
            {

                float distanceA = Vector3.Distance(a.Position, gridWorldPosition);
                float distanceB = Vector3.Distance(b.Position, gridWorldPosition);

                if (distanceA < distanceB)
                    return -1;
                else if (distanceB < distanceA)
                    return 1;
                return 0;
            }

        }

        private class ProgrammableBlock
        {

            protected Program program;
            private readonly IMyTextSurface textSurface;

            public ProgrammableBlock(Program program, UpdateFrequency updateFrequency = UpdateFrequency.Update1)
            {
                this.program = program;
                program.Runtime.UpdateFrequency = updateFrequency;
                textSurface = program.Me.GetSurface(0);
                textSurface.ContentType = ContentType.TEXT_AND_IMAGE;
            }

            public void Draw(InterstellarCom comm, Map map)
            {
                this.textSurface.WriteText("Message sent : " + comm.EmittedMessageCounter + "\n" + "Message received : " + comm.ReceivedMessageCounter +"\n\nMax Distance : "+map.maxDistance +"\nMax Offset X : "+map.maxOffset.X+"\nMax Offset Y :"+map.maxOffset.Y+"\nMap X : "+map.mapCoordinates.X+"\nMap Y : "+map.mapCoordinates.Y);

                /*
                        if (program.Me.CubeGrid.GridSizeEnum == MyCubeSize.Small)
                            return;

                        using (MySpriteDrawFrame frame = textSurface.DrawFrame())
                        {

                            frame.Add(new MySprite(SpriteType.TEXT, "SolarMap v0.9", new Vector2(10, 100), null, Color.White, null, TextAlignment.LEFT, 0.7f));
                            frame.Add(new MySprite(SpriteType.TEXT, "Instructions:\nCall chain:\nExecution time:", new Vector2(10, 125), null, Color.White, null, TextAlignment.LEFT, 0.6f));

                            frame.Add(new MySprite(SpriteType.TEXT,
                                program.Runtime.CurrentInstructionCount + "/" + program.Runtime.MaxInstructionCount + "\n" +
                                program.Runtime.CurrentCallChainDepth + "/" + program.Runtime.MaxCallChainDepth + "\n" +
                                program.Runtime.LastRunTimeMs.ToString("F2") + " ms\n", new Vector2(200, 125), null, Color.White, null, TextAlignment.LEFT, 0.6f));

                            if (program.shipController.Main == null)
                            {
                                frame.Add(new MySprite(SpriteType.TEXT, "Status", new Vector2(10, 200), null, Color.White, null, TextAlignment.LEFT, 0.7f));
                                frame.Add(new MySprite(SpriteType.TEXT, "- Controller does not exist.", new Vector2(10, 225), null, Color.White, null, TextAlignment.LEFT, 0.6f));
                            }

                        }
                */
            }

        }

        private readonly Dictionary<UpdateFrequency, UpdateType> Update = new Dictionary<UpdateFrequency, UpdateType>
                {
                    { UpdateFrequency.Update1, UpdateType.Update1 },
                    { UpdateFrequency.Update10, UpdateType.Update10 }
                };

        private class InterstellarCom
        {
            string broadCastTag = "Interstellar Comm Channel";
            IMyBroadcastListener myBroadcastListener;
            IMyUnicastListener myUnicastListener;
            IMyIntergridCommunicationSystem IGC;
            Program Program;
            public int ReceivedMessageCounter { get; set; }
            public int EmittedMessageCounter { get; set; }
            public List<InterstellarModel> interstellarModelCollection { get; set; }

            public InterstellarCom(IMyIntergridCommunicationSystem igc, Program program)
            {
                this.Program = program;
                this.ReceivedMessageCounter = 0;
                this.EmittedMessageCounter = 0;
                this.IGC = igc;

                myBroadcastListener = this.IGC.RegisterBroadcastListener(broadCastTag);
                myBroadcastListener.SetMessageCallback(broadCastTag);
                myUnicastListener = this.IGC.UnicastListener;
                myUnicastListener.SetMessageCallback(broadCastTag);
                interstellarModelCollection = new List<InterstellarModel>();
            }

            public void Sender()
            {
                IGC.SendBroadcastMessage(broadCastTag, "Share your status camarade");
                EmittedMessageCounter++;
            }
            public void Receiver(UpdateType updateSource)
            {
                if ((updateSource & UpdateType.IGC) > 0)
                {
                    BroadcastCallback();
                    UnicastCallback();
                }
            }

            public void BroadcastCallback()
            {
                while (myBroadcastListener.HasPendingMessage)
                {
                    MyIGCMessage incomingMessage = myBroadcastListener.AcceptMessage();
                    if (incomingMessage.Tag == broadCastTag)
                    {
                        if (incomingMessage.Data is string)
                        {
                            var resp = new InterstellarModel()
                            {
                                Source = incomingMessage.Source,
                                ShipPosition = Program.Me.GetPosition(),
                                Name = Program.Me.CubeGrid.Name
                            };
                            var trame = resp.Serialization();
                            Program.Echo(trame);
                            IGC.SendUnicastMessage(incomingMessage.Source, broadCastTag, resp.Serialization());
                            ReceivedMessageCounter++;
                        }
                    }
                }
            }
            public void UnicastCallback()
            {
                while (myUnicastListener.HasPendingMessage)
                {
                    MyIGCMessage incomingMessage = myUnicastListener.AcceptMessage();
                    if (incomingMessage.Tag == broadCastTag)
                    {
                        if (incomingMessage.Data is string)
                        {
                            var trame = incomingMessage.Data as string;
                            Program.Echo(trame);
                            var message = new InterstellarModel(trame);
                            updateList(message);
                            ReceivedMessageCounter++;
                        }
                    }
                }
            }

            public void updateList(InterstellarModel message)
            {
                var ship = this.interstellarModelCollection.Find(x => x.Source == message.Source);
                if (ship != null)
                {
                    this.interstellarModelCollection.Remove(ship);
                }
                message.RegisteredDate = DateTime.Now;
                this.interstellarModelCollection.Add(message);
                Program.Echo("Ship Number : " + interstellarModelCollection.Count);
            }
            public void checkKeepAlive(InterstellarModel ship)
            {
                var timeFromLastCheck = DateTime.Now - ship.RegisteredDate;
                if(timeFromLastCheck.TotalMilliseconds > 1500)
                {
                    this.interstellarModelCollection.RemoveAll(x => x.Source == ship.Source);
                }
            }

            public bool findExistingShip(InterstellarModel item, long source)
            {
                if (item.Source == source)
                    return true;
                return false;
            }
        }

        private class InterstellarModel
        {
            public long Source { get; set; }
            public Vector3D ShipPosition { get; set; }
            public string Name { get; set; }
            public DateTime RegisteredDate { get; set; }

            public InterstellarModel()
            {
            }
            public InterstellarModel(string req)
            {
                var trame = req.Split(';');
                Source = Int64.Parse(trame[0]);
                ShipPosition = new Vector3D(Double.Parse(trame[1]), Double.Parse(trame[2]), Double.Parse(trame[3]));
                Name = trame[4];
            }
            public string Serialization()
            {
                return Source + ";" + ShipPosition.X + ";" + ShipPosition.Y + ";" + ShipPosition.Z + ";" + Name;
            }

        }

        private static class MatrixHelper
        {
            public static Matrix3x3 Multiply(Matrix3x3 m1, Matrix3x3 m2)
            {
                var result = new Matrix3x3(0, 0, 0, 0, 0, 0, 0, 0, 0);
                result.M11 = m1.Col0.X * m2.Col0.X + m1.Col0.Y * m2.Col1.X + m1.Col0.Z * m2.Col2.X;
                result.M12 = m1.Col0.X * m2.Col0.Y + m1.Col0.Y * m2.Col1.Y + m1.Col0.Z * m2.Col2.Y;
                result.M13 = m1.Col0.X * m2.Col0.Z + m1.Col0.Y * m2.Col1.Z + m1.Col0.Z * m2.Col2.Z;

                result.M21 = m1.Col1.X * m2.Col0.X + m1.Col1.Y * m2.Col1.X + m1.Col1.Z * m2.Col2.X;
                result.M22 = m1.Col1.X * m2.Col0.Y + m1.Col1.Y * m2.Col1.Y + m1.Col1.Z * m2.Col2.Y;
                result.M23 = m1.Col1.X * m2.Col0.Z + m1.Col1.Y * m2.Col1.Z + m1.Col1.Z * m2.Col2.Z;

                result.M31 = m1.Col2.X * m2.Col0.X + m1.Col2.Y * m2.Col1.X + m1.Col2.Z * m2.Col2.X;
                result.M32 = m1.Col2.X * m2.Col0.Y + m1.Col2.Y * m2.Col1.Y + m1.Col2.Z * m2.Col2.Y;
                result.M33 = m1.Col2.X * m2.Col0.Z + m1.Col2.Y * m2.Col1.Z + m1.Col2.Z * m2.Col2.Z;

                return result;
            }

            public static Vector3 Multiply(Vector3 v, Matrix3x3 m)
            {
                var result = new Vector3(0, 0, 0);
                result.X = v.X * m.Col0.X + v.Y * m.Col1.X + v.Z * m.Col2.X;
                result.Y = v.X * m.Col0.Y + v.Y * m.Col1.Y + v.Z * m.Col2.Y;
                result.Z = v.X * m.Col0.Z + v.Y * m.Col1.Z + v.Z * m.Col2.Z;
                return result;
            }
        }
    }
}
